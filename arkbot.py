import discord
import valve.source.master_server
import valve.source.a2s
import json
import datetime
import asyncio
import os

STEAM_SETTINGS_FILE= "steam_settings.json"
DISCORD_BOT_SETTINGS_FILE = "discord_bot_settings.json"
ARK_SERVERLIST_SETTINGS_FILE = "ark_serverlist.json"
arkbot = discord.Client()

########################################################################################################
# Helper functions
########################################################################################################


########################################################################################################
# Game functions
########################################################################################################
def arkDisplayHelp():
  returnString = "Available commands:\n"
  returnString += "{:<40}{:<15}\n".format("-help", "Display this help text")
  returnString += "{:<40}{:<15}\n".format("-players", "Show players online on ragnarok")
  returnString += "{:<40}{:<15}\n".format("-watchlist", "Display players on watchlist")
  returnString += "{:<40}{:<15}\n".format("-watch <user>", "Add <user> to watchlist")
  returnString += "{:<40}{:<15}\n".format("-unwatch <user>", "Remove <user> from watchlist")
  returnString += "{:<40}{:<15}\n".format("-watchon", "Enable watchlist")
  returnString += "{:<40}{:<15}\n".format("-watchoff", "Disable watchlist")
  return returnString

# Fetch player info from steam servers
def arkGetOnlineUsers(arkServer):
  with valve.source.a2s.ServerQuerier((arkServer["ip"],arkServer["port"])) as server:
      return (server.info(),server.players())

# Convert player info to readable string
def arkConvOnlineUsers(server,players,arkServer):
  returnString = "--- {} server ---\n".format(server['server_name'])
  returnString += "{:<40}{:<15}\n".format("Name:","Duration:")
  for player in sorted(players["players"], key=lambda k: k['duration'], reverse=True):
    if player["name"] != "": 
      returnString += "{:<40}{:<15}\n".format(player["name"],str(datetime.timedelta(seconds=round(player["duration"]))))
  return returnString

########################################################################################################
# Game and user list
########################################################################################################


########################################################################################################
# Startup
########################################################################################################

@arkbot.event
async def on_ready():
  print('Connected as:')
  print(arkbot.user.name)
  print(arkbot.user.id)
  
########################################################################################################
# Message handling
########################################################################################################
@arkbot.event
async def on_message(message):
  if message.content.startswith('-players'):
    commandInput = message.content.split()
    if len(commandInput) == 1:
      for arkServer in arkServers:
        (server,players)       = arkGetOnlineUsers(arkServer)
        playersString = arkConvOnlineUsers(server,players,arkServer)
        await arkbot.send_message(message.channel, "```{}```".format(playersString))

  elif message.content.startswith('-watchon'):
    await arkbot.send_message(message.channel, "To be implemented!".format(playersString))

  elif message.content.startswith('-watchoff'):
    await arkbot.send_message(message.channel, "To be implemented!".format(playersString))

  elif message.content.startswith('-watchlist'):
    await arkbot.send_message(message.channel, "To be implemented!".format(playersString))

  elif message.content.startswith('-watch'):
    await arkbot.send_message(message.channel, "To be implemented!".format(playersString))

  elif message.content.startswith('-unwatch'):
    await arkbot.send_message(message.channel, "To be implemented!".format(playersString))

  elif message.content.startswith('-help'):
    await arkbot.send_message(message.channel, "```{}```".format(arkDisplayHelp()))

# Import server list
with open(ARK_SERVERLIST_SETTINGS_FILE,'r') as jsonFile:
  arkServers = json.load(jsonFile)

# Import discord bot settings
with open(DISCORD_BOT_SETTINGS_FILE,'r') as jsonFile:
  botSettings = json.load(jsonFile)

arkbot.run(botSettings['token'])